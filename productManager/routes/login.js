var express = require("express");
var router = express.Router();
var db = require("../db/db-users-nodejs");

router.get("/", function(req, res) {
  res.render("view-login.ejs");
});

router.get("/register", function(req, res) {
  res.render("view-register.ejs");
});

router.post("/registered", function(req, res) {
  db.insertUser(req.body.login, req.body.password);
  res.render("view-login.ejs");
});

router.post("/login", function(req, res) {
  db.login(req.body.username, req.body.password, (err, rows) => {
    console.log("taille selection", rows.length);
    console.log("error", err);
    if (rows.length > 0) {
      res.redirect("/home");
    } else {
      res.redirect("/");
    }
  });
});

module.exports = router;
