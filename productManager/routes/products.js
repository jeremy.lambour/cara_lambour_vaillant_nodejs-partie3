var express = require("express");
var router = express.Router();
var dbProducts = require("../db/db-products-nodejs");
var dbOpinons = require("../db/db-opinions-nodejs");

router.get("/", function(req, res) {
  let callback = (err, rows) => {
    if (err) {
      console.log(err);
    } else {
      rows = rows.filter(x => {
        if (x.id !== null) {
          return x;
        }
      });

      console.log(rows);
      res.render("view-product.ejs", { produits: rows });
    }
  };
  dbProducts.selectAllProduct(callback);
});

router.get("/product/:id", function(req, res) {
  var produit = [];

  let callback = (err, row) => {
    if (err) {
      console.log(err);
    } else {
      produit = row[0];
      dbOpinons.selectAllCommentaryById(req.params.id, showView);
    }
  };

  let showView = (err, row) => {
    if (err) {
      console.log(err);
    } else {
      console.log(row);
      res.render("view-product-sheet.ejs", { produit: produit, comments: row });
    }
  };

  dbProducts.selectProductById(req.params.id, callback);
});

module.exports = router;
