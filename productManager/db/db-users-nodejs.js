//Module base de données
var sqlite = require("sqlite3");
var db = new sqlite.Database("./db/db-cara-nodejs.db3");

/* Users */
function insertUser(nom, password) {
  db.run("insert into users(nom,password) values(?,?)", [nom, password]);
}

function selectAllUsers(cb) {
  db.each("SELECT * FROM users", cb);
}

function login(username, password, cb) {
  db.all(
    "SELECT * FROM users where nom = ? and password = ?",
    [username, password],
    cb
  );
}

//Export fonctions base utilisateurs
exports.insertUser = insertUser;
exports.login = login;
exports.selectAllUsers = selectAllUsers;
