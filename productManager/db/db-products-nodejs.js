//Module base de données
var sqlite = require("sqlite3");
var db = new sqlite.Database("./db/db-cara-nodejs.db3");

/* Products */
function insertProduct(nom, description, prix, src) {
  db.run("insert into produits values(?,?,?,?)", [nom, description, prix, src]);
}

function selectAllProduct(cb) {
  return db.all("SELECT * FROM produits", cb);
}

function selectProductById(id, cb) {
  db.all("SELECT * FROM produits where id = ?", [id], cb);
}

//Export fonctions base produits
exports.insertProduct = insertProduct;
exports.selectAllProduct = selectAllProduct;
exports.selectProductById = selectProductById;
