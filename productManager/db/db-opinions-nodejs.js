//Module base de données
var sqlite = require("sqlite3");
var db = new sqlite.Database("./db/db-cara-nodejs.db3");

/* Avis */
function insertCommentary(idProduct,idUser,commentary)
{
    db.run("insert into opinions (idProduct,idUser,commentary) values(?,?,?)",[idProduct,idUser,commentary]);
}

function selectAllCommentaryById(id,cb)
{
    return db.all("SELECT * FROM opinions " +
    "join users on users.id = opinions.idUser WHERE idProduct = ?", [id], cb);
}

//Export fonctions base produits
exports.insertCommentary = insertCommentary;
exports.selectAllCommentaryById = selectAllCommentaryById;


