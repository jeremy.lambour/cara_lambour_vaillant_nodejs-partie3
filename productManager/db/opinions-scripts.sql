create table opinions(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    idProduct INT, 
    idUser INT,
    commentary TEXT,
    FOREIGN KEY(idProduct) REFERENCES produits(id),
    FOREIGN KEY(idUser) REFERENCES users(id)
);  

PRAGMA foreign_keys = 1;

insert into opinions(idProduct,idUser,commentary) 
values(1,1,"Apres avoir mangé un chocolat on ne sait pas s'arreter ! ");

insert into opinions(idProduct,idUser,commentary) 
values(1,4,"Délicieux, même si les mon chéri contiennent un peu trop d'alcool");

insert into opinions(idProduct,idUser,commentary) 
values(2,24, "Trés bon produit, attention tout de même c'est une arrrrme ");

insert into opinions(idProduct,idUser,commentary) 
values(2,232123, "Trés bon produit, attention tout de même c'est une arrrrme ");
