## CAR_NODEJS

<details> 
<summary>

## Installation

</summary>
* Télécharger la version LTS Linux Binaries 64-bit de NODE_JS
    > [ici](https://nodejs.org/en/download/)
* Ajouter nodejs à votre local 
    > /local/nom_etudiant
* Modifier votre path 
    > vi .bashrc

    > export PATH="$PATH":"/local/nom_etudiant/node-votre-version/bin"

- Assurez-vous que la commande fonctionne > npm -v
  </details>

<details> 
<summary>

## Introduction

</summary>
Aujourdhui, vous allez réaliser une appplication .....
</details>

<details> 
<summary>

## Partie 1 - Création d'une application basique avec NodeJS

</summary>
Dans cette partie, vous allez découvrir comment mettre en place votre premier serveur nodeJS ainsi que concevoir vos premiers modules

Pour se faire nous allons développer une petite brique applicative permettant de créer des comptes utilisateurs et s'authentifier.

Commencer par cloner le repository Git afin de récupérer les fichiers nécessaire pour cette partie.

Vous devriez avoir une arborescence comme suit :

```bash

├── db
│   ├── db-cara-nodejs.db3
│   └── db-module-nodejs.js
├── package.json
├── README.md
├── server
│   └── server.js
└── template
    ├── template-login.html
    └── template-register.html
```

executer ensuite la commande `npm install` afin d'installer les dépendances requises pour cette partie

- 1 Création des routes et des templates des pages de l'application

Dans un premier temps, nous allons commencer par créer la base de notre application c'est à dire déclarer un serveur node.js

Pour se faire commencer par compléter le fichier `server.js` en lui déclarant un serveur qui écoutera le port 8080

Pour rappel, pour créer un serveur simple il vous suffit de créer un fichier Javascript avec le code suivant :

```javascript
var http = require("http");
var url = require("url");

var server = http.createServer(function(req, res) {
  var page = url.parse(req.url).pathname;
  res.writeHead(200, { "Content-Type": "text/html" });
  res.write("Bienvenue dans la présentation CAR-NodeJS");
  res.end();
});
server.listen(8080);
```

ensuite pour le lancer il vous suffit de lancer la commande `node server.js`

En ouvrant votre navigateur vous devriez voir la chose suivante :
//TODO insérer image rendu visuel

- **1 Création des pages de notre application**

Dans cette partie nous allons nous concentrer sur l'élaboration des différentes vues et routes associé à l'application.
Elles sont au nombre de trois :

- Une page permettant de s'authentifier qui sera relié lors de l'appel de l'url `/login`
- Une page permettant la création d'un compte qui sera renvoyer lors de l'appel de l'url `/register`
- Une page à retourner lorsque l'utilisateur entre une URL non connu par le serveur

Des templates préfaites sont disponibles dans le dossier `template`

Une fois cela effectué, lancer l'application.
Vous devriez être en mesure d'accèder à vos page de création et authentification via les urls `http://localhost:8080/login` et `http://localhost:8080/register` et renvoyer à la page 404 lorsque vous entrez un url inconnu

- **2 création du module d'appel à la base de donnée**

Après avoir défini nos routes, il faut maintenant pouvoir créer des utilisateurs qui seront sauvegardés dans une base de données.

Pour ce faire, créer un fichier js que vous nommerez `db-module-nodejs.js` dans le dossier `db` qui permettra de déclarer un module pour la gestion des utilisateurs de notre application

Nous auront besoin de deux fonctions dans ce module :

- `createUser(username,password)` : qui va ajouter en base de données l'utilisateur en spécifiant son username et son mot de passe
- `listAllUsers()` : qui va renvoyer la liste de tous les utilisateurs créer en base

Pour rappel la déclaration de fonction dans un module se fait de la manière suivante :

```javascript
var functionA = () => {
  //Traitement de la fonction
};

exports.functionA = functionA;
```

une fois votre module déclaré, faite ajouter l'appel de la fonction `createUser` lors de l'envoi du formulaire de création d'utilisateur et l'appel de la fonction `listAllUsers()` lorsque l'utilisateur se connecte

lancer de nouveau l'application et procéder à une création de compte puis une authentification.
Vous devriez voir apparaitre la liste de tous les utilisateurs disponibles.

</details>

<details> 
<summary>

## Partie 2 - De la vitamine C avec Express

</summary>
Dans cette partie, l'objectif est d'afficher un d'intégrer notre système d'authentification précedemment créé et d'afficher une page produit en utilisant le framework Express.

Elle permettra également de se familiariser avec une nouvelle architecture de projet généré avec le scaffolder intégré à Express.

Lien utile : [Documentation Express](https://expressjs.com/fr/starter/generator.html)
[Documentation EJS](https://ejs.co/#docs)

- **1 Création de l'arborescence de travail**

Dans un premier temps placer vous dans un dossier vierge et executer les commandes suivantes :

```bash
npm init
npm install express-generator --save
```

Ensuite dans la section `script` du fichier `package.json` ajouter la ligne `express:"express"` afin de pouvoir utiliser la dépendance en locale.

Executer ensuite la commande `npm run express --view=ejs catalogue`

Cette dernière va générer l'arborescence de votre projet.

Si tout c'est bien passé vous devriez avoir une architecture comme suis :

```bash
.
├── bin
├── public
│   ├── images
│   ├── javascripts
│   └── stylesheets
├── routes
└── views
```

Une fois cela effectué, placer vous le dossier `catalogue` et lancer la commande `npm install` afin d'installer les dépendances nécessaires puis `DEBUG=catalogue:* npm start` afin de lancer l'application.

Si en ouvrant votre navigateur à l'adresse `localhost:3000` une page d'accueil express s'affiche c'est que l'installation s'est bien passé.

Vous êtes maintenant prêt pour travailler sur la partie 2.

- **2 Premier pas avec Express**

Dans un premier temps cloner le dépôt afin de récupérer l'ensemble des fichiers nécessaires au dévelopement.

- Placer les images fournis dans le dossier `public/images` de votre arborescence
- le dossier `db` à la racine de votre projet express.
- les fichiers `ejs` dans le dossiers `views`.
- les fichiers `js` fournis dans le dossier `routes`

Toutes les templates sont déjà pré-remplis, seul la page `view-product.ejs` sera à compléter.

- **3 La mise en place des routes**

Tout d'abord avec Express le système de routage est simplifié. En effet, il met à disposition un objet `Router` qui permet d'associer un verb http à une url et une fonction associer.

Exemple : affichage d'une vue de login sur l'url `/login`

```javascript
router.get("/login", function(req, res) {
  res.render("view-login.ejs");
});
```

Essayer de faire la même chose pour afficher la page produit

affichage souhaité :
![alt text](img/pageproduit.png)

Voici la liste des fichiers à modifier :

- `app.js` :
  ajouter la route `/home`

- `view-product.ejs` : page produits à completer. Rappel pour utiliser du JS mettre les balise <%> ou <%=>

```javascript
<%
produits.forEach(function(produit) {});
>
```

- `db-products-nodejs.js` :
  à completer la fonction ci-dessous

```javascript
function selectAllProduct(cb)
});
```

- `products.js` :
  à complèter pour afficher l'ensemble des produits

- `login.js`:
  à complèter pour adapter le système de d'authentification/creation de compte de la partie 1

</details>

# Partie 3 - Votre avis nous intéresse

Dans cette partie nous allons créer, mettre en pratique l'utilisation de la librairie `socket.io` à travers la gestion d'avis sur les différents produits de notre catalogue.

Dans un premier temps, cloner le repository afin de récupérer la dernière version de notre application.

Cette dernière dispose des liens nécessaires pour afficher la page d'un produit.
Vous pouvez retrouver le descriptif des routes dans le fichier `product.js`.

Lien utile : [Documentation Socket.io](https://socket.io/docs/)

- **1 Création du serveur socketIO**
  Avant de passer à la gestion des clients, il nous faut tout d'abord déclarer un serveur d'écoute qui va récupérer les avis envoyés par les différents client et les retransmettre aux autres.

Pour cela placer vous dans le fichier `www` du dossier `bin` et effectuer les actions suivantes :

- Faire écouter un socket.io sur notre serveur
- Créer un `listener` sur la réception d'un événement `avis` qui va récuperer le message et le transmettre à tous les autres clients par le biais d'un autre.

- **2 Gestion de l'envoi des avis vers tous les clients**

Du côté client, complèter le fichier `view-product-sheet.ejs` pour :

- Créer la socket client qui va se connecter au serveur
- Ajouter un listener sur l'événement "avis" afin de pouvoir mettre à jour la liste des avis en temps réel
- Ajouter un emetteur de l'evement "avis" lors du submit du formulaire afin d'envoyer le commentaire à tous les utilisateurs connectés.
- Manipuler le DOM avec JQuery pour afficher les commentaires.
